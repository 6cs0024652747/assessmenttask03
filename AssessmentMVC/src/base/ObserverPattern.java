package base;

public interface ObserverPattern {
    void update(String playerName);
}
