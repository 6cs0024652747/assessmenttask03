package base;

import javax.swing.*;
import java.awt.*;

public class AbominodoView extends JFrame implements ObserverPattern {
    private JTextField nameField;
    private JButton enterButton;

    private AbominodoModel modelInterface = new AbominodoModel();
    private AbominodoController controllerInterface = new AbominodoController(modelInterface, this);

    public AbominodoView() {
        setTitle("Abominodo - Dominoes Puzzle Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Set the background color to pink
        getContentPane().setBackground(new Color(255, 182, 193));

        setLayout(new BorderLayout());

        // Display welcome message
        JOptionPane.showMessageDialog(this, "Welcome To Abominodo - The Best Dominoes Puzzle Game in the Universe. \n"
                + "Version 2.1 (c), Kevan Buckley, 2014");

        // User input
        JPanel inputPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JLabel inputLabel = new JLabel("Please Enter Your Name:");
        nameField = new JTextField(20);
        enterButton = new JButton("Enter");
        enterButton.addActionListener(e -> handleSubmission());
        inputPanel.add(inputLabel);
        inputPanel.add(nameField);
        inputPanel.add(enterButton);

        // Set the background color of the inputPanel to pink
        inputPanel.setBackground(new Color(255, 182, 193));

        add(inputPanel, BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        modelInterface.addObserver(this);
    }

    @Override
    public void update(String playerName) {
        nameField.setText(playerName);
    }

    private void handleSubmission() {
        String name = nameField.getText();

        if (name.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Please enter correct name.", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            controllerInterface.sendNameToBackEnd(name);
            dispose();
        }
    }
}
