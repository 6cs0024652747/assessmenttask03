package base;

public class AbominodoController {
    private AbominodoModel model;
    private AbominodoView view;

    public AbominodoController(AbominodoModel model, AbominodoView view) {
        this.model = model;
        this.view = view;
    }

    public void showView() {
        view.setVisible(true);
    }

    public void sendNameToBackEnd(String name) {
        model.setPlayerName(name);
        model.sendNameToBackEnd(name);
    }
}
