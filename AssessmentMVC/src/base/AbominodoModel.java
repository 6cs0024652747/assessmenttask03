package base;
import java.util.ArrayList;
import java.util.List;
public class AbominodoModel {
    private String playerName;
    private List<ObserverPattern> observers = new ArrayList<>();

    public AbominodoModel() {
    }
    public String getPlayerName() {
        return playerName;
    }
    public void setPlayerName(String playerName) {
        this.playerName = playerName;
        notifyObservers();
    }
    public void addObserver(ObserverPattern observer) {
        observers.add(observer);
    }
    public void removeObserver(ObserverPattern observer) {
        observers.remove(observer);
    }
    private void notifyObservers() {
        for (ObserverPattern observer : observers) {
            observer.update(playerName);
        }
    }
    public void sendNameToBackEnd(String name) {
        System.out.printf("Welcome %s! Thanks for playing Abominodo.\n ----PRESS ENTER TO START THE GAME----", name);
    }
}
